#!/bin/bash

set -e

AUTH_LOG="$(cat /tmp/session.log)"
WEBHOOK=""

if [[ *$AUTH_LOG* == *"Accepted"* ]]; then
SSH_USER="$(cat /tmp/session.log | sed -nE '/Accepted.* ssh2/ s/^.*for ([^ ]+).*$/\1/p')"
SSH_IP="$(cat /tmp/session.log | sed -nE '/Accepted.* ssh2/ s/^.*from ([^ ]+).*$/\1/p')"
curl -H "Content-Type: application/json" -d "{
            \"username\": \"NEW SSH CONNEXION\", 
            \"avatar_url\": \"https://cdn-icons-png.flaticon.com/512/3596/3596089.png\",
            \"embeds\": [
                {
                \"title\": \"$HOSTNAME\",
                \"color\": 14177041,
                \"description\": \":exclamation: **SSH** \n \n USER: $SSH_USER \n FROM: $SSH_IP \"
                }
            ]
}" $WEBHOOK 
fi

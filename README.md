![BANNER](.gitlab/banner.png)

Simple bash script to send an SSH connection success alert in Discord.

## Setup
### In Discord
Create a Discord Webhook : [Tutorial here](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks)

### In your server
* Clone this repos
* Insert your Webhook URL in the variable $WEBHOOK in `discord.sh`

```shell
WEBHOOK="https://discord.com/api/webhooks/XXXXXXX/XXXXXXXXXXX"
```
* Start `setup.sh`
* Start ssh-alert-notification service
```sh
systemctl start ssh-alert-notification.service
```
* Enjoy :D

## Example

![EXAMPLE](.gitlab/example.png)

## TODO
- [x] SSH access
- [ ] Redesign SSH notification
  - [ ] More explicit and detail
- [x] Probe in daemon --> systemd
- [x] System check

## Tree
    .
    ├── README.md
    ├── ressources
    │   ├── discord.sh
    │   ├── probe.sh
    │   └── ssh-alert-notification.service
    └── setup.sh
     
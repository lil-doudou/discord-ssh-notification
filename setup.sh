#!/bin/bash

set -e

# COLOR
RED='\e[91m'
BLUE='\e[94m'
YELLOW='\e[93m'
GREEN='\e[92m'
NC='\e[39m'


function system_check {
    LINUX_OS="$(cat /etc/os-release | awk -F'=' '{print $2}' | awk 'NR<2')"
    if [[ $LINUX_OS != *"Debian"* ]]; then
        echo -e "${RED}[!] This script must be run on Debian OS${NC}"
        exit 1
    fi
}

function root_check {
    if [[ $EUID != 0 ]]; then
        echo -e "${RED}[!] This script must be run as root${NC}" 
        exit 1
    fi
}

function env_install {
    echo -e "${GREEN} Environnement install ${NC}"

    cp $PWD/ressources/discord.sh /root/
    cp $PWD/ressources/probe.sh /root/

    chmod u+x /root/discord.sh
    chmod u+x /root/probe.sh

    chown root:root /root/discord.sh
    chown root:root /root/probe.sh

    # Copy ssh-alert-probe.service in /etc/systemd/system/
    cp $PWD/ressources/ssh-alert-notification.service /etc/systemd/system/
}

function main {
    echo -e "
██████╗ ██╗███████╗ ██████╗ ██████╗ ██████╗ ██████╗     ███████╗███████╗██╗  ██╗         
██╔══██╗██║██╔════╝██╔════╝██╔═══██╗██╔══██╗██╔══██╗    ██╔════╝██╔════╝██║  ██║         
██║  ██║██║███████╗██║     ██║   ██║██████╔╝██║  ██║    ███████╗███████╗███████║         
██║  ██║██║╚════██║██║     ██║   ██║██╔══██╗██║  ██║    ╚════██║╚════██║██╔══██║         
██████╔╝██║███████║╚██████╗╚██████╔╝██║  ██║██████╔╝    ███████║███████║██║  ██║         
╚═════╝ ╚═╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝     ╚══════╝╚══════╝╚═╝  ╚═╝         
                                                                                         
███╗   ██╗ ██████╗ ████████╗██╗███████╗██╗ ██████╗ █████╗ ████████╗██╗ ██████╗ ███╗   ██╗
████╗  ██║██╔═══██╗╚══██╔══╝██║██╔════╝██║██╔════╝██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║
██╔██╗ ██║██║   ██║   ██║   ██║█████╗  ██║██║     ███████║   ██║   ██║██║   ██║██╔██╗ ██║
██║╚██╗██║██║   ██║   ██║   ██║██╔══╝  ██║██║     ██╔══██║   ██║   ██║██║   ██║██║╚██╗██║
██║ ╚████║╚██████╔╝   ██║   ██║██║     ██║╚██████╗██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║
╚═╝  ╚═══╝ ╚═════╝    ╚═╝   ╚═╝╚═╝     ╚═╝ ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝
                                                                                         
"

    system_check
    root_check
    env_install
}

main